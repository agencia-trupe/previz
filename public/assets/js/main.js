$('document').ready( function(){

	if($('#banners').length){
		$('#banners #target').cycle({
			activePagerClass: 'activeSlide',
			pagerAnchorBuilder : function(index, DOMelement){
				return '<a href="#"></a>';
			},
			pause : true,
			pager : $('#banners-nav')
		});
	}

	$('#form-contato').submit( function(e){
		if($('#input-nome').val() == ''){
			alert('Informe seu nome!');
			e.preventDefault();
			return false;
		}
		if($('#input-email').val() == ''){
			alert('Informe seu e-mail!');
			e.preventDefault();
			return false;
		}
		if($('#input-mensagem').val() == ''){
			alert('Informe sua mensagem!');
			e.preventDefault();
			return false;
		}
	});

});
