@section('conteudo')
	
	<h1 class="titulo-secao clientes">
		<div class="centro">
			<div class="icone"><span></span></div>
			<div class="texto"><span>CLIENTES E PARCEIROS</span></div>
		</div>
	</h1>

	<div class="listaclientes centro">
		@if(sizeof($clientes) > 0)
			@foreach($clientes as $cliente)
				@if($cliente->link)
					<a href="{{$cliente->link}}" target="_blank" title="{{$cliente->titulo}}">
						<img src="assets/images/clientes/{{$cliente->imagem}}" alt="{{$cliente->titulo}}">
					</a>
				@else
					<div title="{{$cliente->titulo}}">
						<img src="assets/images/clientes/{{$cliente->imagem}}" alt="{{$cliente->titulo}}">
					</div>
				@endif
			@endforeach
		@endif
	</div>

@stop