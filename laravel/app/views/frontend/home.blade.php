@section('conteudo')
	
	<div id="banners">
		<div class="centro">
			<div id="faixa"></div>
			<div id="target">
				@if(sizeof($banners) > 0)
					@foreach($banners as $banner)
						<a href="{{$banner->link}}" title="{{$banner->titulo}}" class="banner">
							<h1>{{$banner->titulo}}</h1>
							<span>...</span>
							<p>{{$banner->texto}}</p>
						</a>
					@endforeach
				@endif
			</div>
			<div id="banners-nav"></div>
		</div>
	</div>

	<div id="chamadas">
		@if(sizeof($chamadas) > 0)
			@foreach($chamadas as $k => $chamada)
				<a href="{{$chamada->link}}" title="{{$chamada->titulo}}" class="chamada-{{$k}}">
					<h2>{{$chamada->titulo}}</h2>
					<p>{{$chamada->texto}}</p>
					<div class="saiba-mais"><span>saiba mais &raquo;</span></div>
				</a>
			@endforeach
		@endif
	</div>
@stop
