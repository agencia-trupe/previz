@section('conteudo')
	
	<h1 class="titulo-secao contato">
		<div class="centro">
			<div class="icone"><span></span></div>
			<div class="texto"><span>CONTATO</span></div>
		</div>
	</h1>

	<div class="centro">
		<h2>Envie-nos uma mensagem</h2>
		@if(Session::has('enviado'))
			<div class="enviado">
				Sua mensagem foi enviada com sucesso!
			</div>
		@else 		
			<form action="{{URL::route('contato.enviar')}}" method="post" id="form-contato">
				<div class="coluna1">
					<input type="text" name="nome" placeholder="Nome" required id="input-nome">
					<input type="email" name="email" placeholder="E-mail" required id="input-email">
					<input type="text" name="telefone" placeholder="Telefone" id="input-telefone">
				</div>
				<div class="coluna2">
					<textarea name="mensagem" placeholder="Mensagem" id="input-mensagem" required></textarea>
				</div>
				<div class="enviar">
					<input type="submit" value="enviar &raquo;">
				</div>
			</form>
		@endif
	</div>

@stop