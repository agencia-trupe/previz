<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=980,initial-scale=1">
    
    <meta name="keywords" content="" />

	<title>Previz - Consultoria Previdenciária</title>
	<meta name="description" content="">
	<meta property="og:title" content="Previz - Consultoria Previdenciária"/>
	<meta property="og:description" content=""/>

    <meta property="og:site_name" content="Previz"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/fancybox/source/jquery.fancybox',
		'css/base',
		'css/'.str_replace('-', '', Route::currentRouteName())
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>	
	
	@if(App::environment()=='local')
		<?=Assets::JS(array('vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('vendor/modernizr/modernizr'))?>
	@endif

	<script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
		// Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-', '');
		// ga('send', 'pageview');
	</script>
</head>
<body>

	<header>
		<div class="centro">
			<a href="home" title="Página Inicial" id="logo-link">
				<img src="assets/images/layout/marca-previz.png" alt="Previz - Consultoria Previdenciária">
			</a>
			<nav>
				<ul>
					<li><a href="home" title="HOME" @if(str_is('home*', Route::currentRouteName())) class="ativo" @endif>HOME</a></li>
					<li><a href="quem-somos" title="QUEM SOMOS" @if(str_is('quemsomos*', Route::currentRouteName())) class="ativo" @endif>QUEM SOMOS</a></li>
					<li><a href="solucoes-para-empresas" title="SOLUÇÕES PARA EMPRESAS" @if(str_is('solucoesempresas*', Route::currentRouteName())) class="ativo" @endif>SOLUÇÕES PARA EMPRESAS</a></li>
					<li><a href="solucoes-para-pessoas" title="SOLUÇÕES PARA PESSOAS" @if(str_is('solucoespessoas*', Route::currentRouteName())) class="ativo" @endif>SOLUÇÕES PARA PESSOAS</a></li>
					<li><a href="responsabilidade-social" title="RESPONSABILIDADE SOCIAL" @if(str_is('responsabilidade*', Route::currentRouteName())) class="ativo" @endif>RESPONSABILIDADE SOCIAL</a></li>
					<li><a href="clientes-e-parceiros" title="CLIENTES E PARCEIROS" @if(str_is('clientes*', Route::currentRouteName())) class="ativo" @endif>CLIENTES E PARCEIROS</a></li>
					<li><a href="contato" title="CONTATO" @if(str_is('contato*', Route::currentRouteName())) class="ativo" @endif>CONTATO</a></li>					
				</ul>
			</nav>

			@if(isset($contato) && isset($contato->facebook) && $contato->facebook != '')
				<a href="{{$contato->facebook}}" target="_blank" title="Facebook" id="link-fb">
					<img src="assets/images/layout/icone-facebook.png" alt="Facebook">
				</a>
			@endif
		</div>
	</header>

	<!-- Conteúdo Principal -->
	<div class="main {{ 'main-'. str_replace('-', '', Route::currentRouteName()) }}">
		@yield('conteudo')
	</div>

	<footer>
		<div id="faixa-contato">
			<div class="centro">
				<div class="info">
					@if(isset($contato) && isset($contato->telefone) && $contato->telefone != '')
						<div class="telefone">{{$contato->telefone}}</div>
					@endif
					@if(isset($contato) && isset($contato->email) && $contato->email != '')
						<div class="email"><a href="mailto:{{$contato->email}}" title="Envie um e-mail">{{$contato->email}}</a></div>
					@endif
				</div>
			</div>
		</div>
		<div id="assinatura">
			<p>
				&copy; {{Date('Y')}} Previz - Todos os direitos reservados | <a href="http://www.trupe.net" target="_blank" title="Criação de Sites: Trupe Agência Criativa">Criação de Sites: Trupe Agência Criativa <img src="assets/images/layout/icone-trupe.png" alt="Criação de Sites: Trupe Agência Criativa"></a>
			</p>
		</div>
	</footer>

	<?=Assets::JS(array(
		'vendor/jquery.cycle/jquery.cycle.all',
		'vendor/fancybox/source/jquery.fancybox',
		'js/main'
	))?>

</body>
</html>
