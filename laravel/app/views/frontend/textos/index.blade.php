@section('conteudo')
	
	<h1 class="titulo-secao {{$texto->slug}}">
		<div class="centro">
			<div class="icone"><span></span></div>
			<div class="texto"><span>{{$texto->titulo}}</span></div>
		</div>
	</h1>

	<div class="detalhe-texto centro">
		<section>
			{{$texto->texto}}
		</section>
		<div class="imagem {{$texto->slug}}"></div>
	</div>

@stop
