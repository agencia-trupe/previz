@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.textos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				<div class="form-group">
					<label for="inputtexto">Texto</label>
					<textarea name="texto" class="form-control txtcke" id="inputtexto" >{{$registro->texto }}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.textos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop