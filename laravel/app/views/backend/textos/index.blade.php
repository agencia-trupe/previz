@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Textos @if(sizeof(Texto::all()) < $limiteInsercao) <a href='{{ URL::route('painel.textos.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Texto</a> @endif
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Título</th>
				<th>texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
				<td>{{ Str::words(strip_tags($registro->texto), 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.textos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>					
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop