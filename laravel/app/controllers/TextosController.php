<?php

use \Texto;

class TextosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function quemSomos(){
		$this->layout->content = View::make('frontend.textos.index')->with('texto', Texto::where('slug', '=', 'quem-somos')->first());
	}

	public function empresas(){
		$this->layout->content = View::make('frontend.textos.index')->with('texto', Texto::where('slug', '=', 'solucoes-para-empresas')->first());
	}

	public function pessoas(){
		$this->layout->content = View::make('frontend.textos.index')->with('texto', Texto::where('slug', '=', 'solucoes-para-pessoas')->first());
	}

	public function social(){
		$this->layout->content = View::make('frontend.textos.index')->with('texto', Texto::where('slug', '=', 'responsabilidade-social')->first());
	}

}
