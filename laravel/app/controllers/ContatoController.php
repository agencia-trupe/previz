<?php

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index(){
		$this->layout->content = View::make('frontend.contato.index')->with('contato', \Contato::first());
	}

	public function enviar()
	{
		$data['nome'] = \Input::get('nome');
		$data['email'] = \Input::get('email');
		$data['telefone'] = \Input::get('telefone');
		$data['mensagem'] = \Input::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem']){
			\Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('contato@previz.com.br', 'Previz')
			    		->subject('Contato via site')
			    		->bcc('bruno@trupe.net')
			    		->replyTo($data['email'], $data['nome']);
			});
		}

		Session::flash('enviado', true);
		return \Redirect::route('contato');
	}
}
