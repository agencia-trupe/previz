<?php

use \Banner, \Chamada;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.home')->with('banners', Banner::ordenado()->get())
															->with('chamadas', Chamada::ordenado()->get());
	}

}
