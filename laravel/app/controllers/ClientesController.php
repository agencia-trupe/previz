<?php

use \Cliente;

class ClientesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index(){
		$this->layout->content = View::make('frontend.clientes.index')->with('clientes', Cliente::ordenado()->get());
	}
}
