<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
				'telefone' => '11 5011-0932 | 11 5078-7012',
				'email' => 'contato@previz.com.br',
            )
        );

        DB::table('contato')->insert($data);
    }

}
