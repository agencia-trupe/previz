<?php

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('quem-somos', array('as' => 'quemsomos', 'uses' => 'TextosController@quemSomos'));
Route::get('solucoes-para-empresas', array('as' => 'solucoesempresas', 'uses' => 'TextosController@empresas'));
Route::get('solucoes-para-pessoas', array('as' => 'solucoespessoas', 'uses' => 'TextosController@pessoas'));
Route::get('responsabilidade-social', array('as' => 'responsabilidade', 'uses' => 'TextosController@social'));
Route::get('clientes-e-parceiros', array('as' => 'clientes', 'uses' => 'ClientesController@index'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::post('contato', array('as' => 'contato.enviar', 'uses' => 'ContatoController@enviar'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('textos', 'Painel\TextosController');
	Route::resource('chamadas', 'Painel\ChamadasController');
	Route::resource('banners', 'Painel\BannersController');
	Route::resource('contato', 'Painel\ContatoController');
	Route::resource('clientes', 'Painel\ClientesController');
//NOVASROTASDOPAINEL//
});